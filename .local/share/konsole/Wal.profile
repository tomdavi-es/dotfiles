[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=colors-konsole
Font=Fira Code,11,-1,5,50,0,0,0,0,0,Regular
UseFontLineChararacters=true

[General]
Name=Wal
Parent=FALLBACK/
ShowTerminalSizeHint=false

[Scrolling]
ScrollBarPosition=2
